## Model extension for Laravel

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Mix and match traits/interfaces as needed using the base models

##### VO Model
`Smorken\Model\VO`

##### Eloquent Model (extend)
`Smorken\Model\Eloquent`
