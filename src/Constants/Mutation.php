<?php

namespace Smorken\Model\Constants;

enum Mutation
{
    case GET;

    case SET;

    case BOTH;

    public function label(): string
    {
        return strtolower($this->name);
    }
}
