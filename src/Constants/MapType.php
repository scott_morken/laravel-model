<?php

namespace Smorken\Model\Constants;

enum MapType
{
    case VIRTUAL;
    case STRING;

    public function label(): string
    {
        return strtolower($this->name);
    }
}
