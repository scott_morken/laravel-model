<?php

namespace Smorken\Model\Constants;

enum MappingDirection
{
    case FORWARD;

    case INVERSE;

    public function label(): string
    {
        return strtolower($this->name);
    }
}
