<?php

namespace Smorken\Model\Constants;

enum OrderDirection: string
{
    case ASCENDING = 'asc';

    case DESCENDING = 'desc';
}
