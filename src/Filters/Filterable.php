<?php

namespace Smorken\Model\Filters;

use Smorken\Support\Contracts\Filter;

class Filterable
{
    public function __construct(
        public Filter $filter,
        public iterable $handlers
    ) {}
}
