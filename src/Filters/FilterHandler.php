<?php

namespace Smorken\Model\Filters;

use Smorken\Model\QueryBuilders\InvokableQueryScope;

class FilterHandler
{
    public function __construct(
        public string $filterKey,
        public string|\Closure $queryMethod,
        public string $builderClass = InvokableQueryScope::class
    ) {}
}
