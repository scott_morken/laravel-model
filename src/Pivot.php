<?php

namespace Smorken\Model;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Concerns\HasAttributes;
use Smorken\Model\Concerns\WithFriendlyKeys;
use Smorken\Model\Contracts\Model;
use Smorken\Model\QueryBuilders\Builder;

#[\AllowDynamicProperties]
abstract class Pivot extends \Illuminate\Database\Eloquent\Relations\Pivot implements Model
{
    use HasAttributes, WithFriendlyKeys;

    /** @use HasBuilder<Builder<static>> */
    use HasBuilder;

    /**
     * @var class-string<\Smorken\Model\QueryBuilders\Builder<*>>
     */
    protected static string $builder = Builder::class;
}
