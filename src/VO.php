<?php

namespace Smorken\Model;

use Smorken\Model\Concerns\HasAttributes;

class VO extends BaseVO
{
    use HasAttributes;

    public function __construct(array $attributes = [])
    {
        $this->fill($attributes);
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function __isset(string $key): bool
    {
        return $this->offsetExists($key);
    }

    public function isRelation($key)
    {
        return false;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->attributes[$offset]);
    }

    public function relationLoaded($key)
    {
        return false;
    }

    public function __sleep()
    {
        $this->mergeAttributesFromCachedCasts();

        $this->classCastCache = [];
        $this->attributeCastCache = [];
        // set to empty arrays to avoid closure serialization
        if (property_exists($this, 'cacheRepository')) {
            $this->cacheRepository = null;
        }
        $this->attributesProviders = [];

        return array_keys(get_object_vars($this));
    }
}
