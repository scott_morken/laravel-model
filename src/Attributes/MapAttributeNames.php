<?php

declare(strict_types=1);

namespace Smorken\Model\Attributes;

use Attribute;
use Smorken\Model\Contracts\Attributes\Mapper;

#[Attribute(Attribute::TARGET_CLASS)]
class MapAttributeNames
{
    public function __construct(protected Mapper $mapper, protected ?Mapper $inverseMapper = null) {}

    public function getMap(): array
    {
        return $this->mapper->getMap();
    }

    public function inverse(string $key): string
    {
        if ($this->inverseMapper) {
            return $this->inverseMapper->map($key); // inverse mapper uses map method
        }

        return $this->mapper->inverse($key);
    }

    public function map(string $key): string
    {
        return $this->mapper->map($key);
    }
}
