<?php

namespace Smorken\Model\Attributes\Concerns;

use Smorken\Model\Contracts\Attributes\AttributesProvider;

trait HasAttributesProviders
{
    /**
     * @var array<string, AttributesProvider>
     */
    protected array $attributesProviders = [];

    protected function getAttributesProvider(string $attributeClass): AttributesProvider
    {
        if (! ($this->attributesProviders[$attributeClass] ?? false)) {
            $this->attributesProviders[$attributeClass] = new \Smorken\Model\Attributes\Helpers\AttributesProvider(
                $attributeClass,
                $this
            );
        }

        return $this->attributesProviders[$attributeClass];
    }
}
