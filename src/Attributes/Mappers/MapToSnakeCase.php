<?php

declare(strict_types=1);

namespace Smorken\Model\Attributes\Mappers;

use Illuminate\Support\Str;
use Smorken\Model\Contracts\Attributes\Mapper;

class MapToSnakeCase implements Mapper
{
    protected array $original = [];

    public function getMap(): array
    {
        return array_flip($this->original);
    }

    public function inverse(string $key): string
    {
        return $this->original[$key] ?? $key;
    }

    public function map(string $key): string
    {
        $cased = Str::snake($key);
        $this->original[$cased] = $key;

        return $cased;
    }
}
