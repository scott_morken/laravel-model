<?php

declare(strict_types=1);

namespace Smorken\Model\Attributes\Mappers;

use Smorken\Model\Contracts\Attributes\Mapper;

class MapToArray implements Mapper
{
    protected ?array $inverse = null;

    public function __construct(protected array $map = []) {}

    public function getMap(): array
    {
        return $this->getMapArray();
    }

    public function inverse(string $key): string
    {
        return $this->getInverseMapArray()[$key] ?? $key;
    }

    public function map(string $key): string
    {
        return $this->getMapArray()[$key] ?? $key;
    }

    protected function getInverseMapArray(): array
    {
        if ($this->inverse === null) {
            $this->inverse = array_flip($this->getMapArray());
        }

        return $this->inverse;
    }

    protected function getMapArray(): array
    {
        return $this->map;
    }
}
