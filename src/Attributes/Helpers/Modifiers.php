<?php

namespace Smorken\Model\Attributes\Helpers;

use Smorken\Model\Attributes\Modifiers\ViaCallbackModifier;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Attributes\Modifier;

class Modifiers
{
    /**
     * @var array<string,array<string, array<int, \Smorken\Model\Contracts\Attributes\Modifier>>>
     */
    protected array $modifiers = [];

    /**
     * @param  array<string, \Smorken\Model\Contracts\Attributes\Modifier>  $modifiers
     */
    public function __construct(array $modifiers = [])
    {
        $this->setModifiers($this->modifiers($modifiers));
    }

    public function for(string $key, Mutation $type): array
    {
        $for = $this->getModifiers()[$type->label()][$key] ?? [];
        if (! is_array($for)) {
            $for = [$for];
        }

        return $for;
    }

    protected function addModifier(string $key, Modifier|\Closure $modifier): void
    {
        if ($modifier instanceof \Closure) {
            $modifier = new ViaCallbackModifier($modifier);
        }
        if ($modifier->isMutation(Mutation::SET)) {
            $this->modifiers[Mutation::SET->label()][$key][] = $modifier;
        }
        if ($modifier->isMutation(Mutation::GET)) {
            $this->modifiers[Mutation::GET->label()][$key][] = $modifier;
        }
    }

    protected function addModifiers(array $modifiers): void
    {
        /**
         * @var string $key
         * @var \Smorken\Model\Contracts\Attributes\Modifier $modifier
         */
        foreach ($modifiers as $key => $modifier) {
            $this->addModifier($key, $modifier);
        }
    }

    protected function getModifiers(): array
    {
        return $this->modifiers;
    }

    protected function setModifiers(array $modifiers): void
    {
        $this->modifiers = [Mutation::SET->label() => [], Mutation::GET->label() => []];
        /**
         * @var string $mutationTypeOrKey
         * @var \Smorken\Model\Contracts\Attributes\Modifier $modifiersForMutationOrModifier
         */
        foreach ($modifiers as $mutationTypeOrKey => $modifiersForMutationOrModifier) {
            // @phpstan-ignore function.impossibleType
            if (is_array($modifiersForMutationOrModifier)) {
                $this->addModifiers($modifiersForMutationOrModifier);
            } else {
                $this->addModifier($mutationTypeOrKey, $modifiersForMutationOrModifier);
            }
        }
    }

    /**
     * Override this method to load modifiers after instantiation
     */
    protected function modifiers(array $modifiers = []): array
    {
        return [...$this->modifiers, ...$modifiers];
    }
}
