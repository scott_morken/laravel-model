<?php

namespace Smorken\Model\Attributes\Helpers;

use Illuminate\Support\Collection;

class AttributesProvider implements \Smorken\Model\Contracts\Attributes\AttributesProvider
{
    /**
     * @var array<string, array<int, \Attribute>>|array<string, array<string, array<int, \Attribute>>>
     */
    protected array $cached = [
        'class' => [],
        'property' => [],
    ];

    protected array $loaded = ['class' => false, 'property' => false];

    protected \ReflectionClass $reflectionClass;

    public function __construct(
        protected string $attributeClass,
        protected object|string $classToReflect
    ) {
        $this->reflectionClass = new \ReflectionClass($this->classToReflect);
    }

    public function __serialize(): array
    {
        return [
            'attributeClass' => $this->attributeClass,
            'classToReflect' => $this->classToReflect,
        ];
    }

    public function __unserialize(array $data): void
    {
        $this->attributeClass = $data['attributeClass'];
        $this->classToReflect = $data['classToReflect'];
        $this->reflectionClass = new \ReflectionClass($this->classToReflect);
    }

    /**
     * @return array<int, \Attribute>
     */
    public function getClassLevelAttributes(): array
    {
        if ($this->hasClassLevelCached()) {
            return $this->fromCache('class');
        }
        $this->toCache('class', $this->getAttributesArrayForClass());

        return $this->fromCache('class');
    }

    public function getPropertyLevelAttributes(string $propertyName): array
    {
        if (! $this->hasAnyPropertyCached()) {
            $this->toCache('property', $this->getAllPropertyLevelAttributes());
        }

        return $this->fromPropertyCache($propertyName);
    }

    protected function addPropertyAttributesToCollection(\ReflectionProperty $property, Collection $collection): void
    {
        $attributes = $this->getAttributesArrayForProperty($property);
        if ($attributes) {
            $propertyCollection = $this->getPropertyCollection($property->getName(), $collection);
            $propertyCollection->push(...$attributes);
        }
    }

    protected function ensurePropertyCollectionHasPropertyName(string $propertyName, Collection $collection): void
    {
        if (! $collection->has($propertyName)) {
            $collection->put($propertyName, new Collection);
        }
    }

    protected function fromCache(string $key): array
    {
        return $this->cached[$key];
    }

    protected function fromPropertyCache(string $property): array
    {
        return $this->fromCache('property')[$property] ?? [];
    }

    protected function getAllPropertyLevelAttributes(): array
    {
        $attributes = new Collection;
        foreach ($this->getPublicProperties() as $property) {
            $this->addPropertyAttributesToCollection($property, $attributes);
        }

        return $attributes->filter(static fn (Collection $items) => $items->isNotEmpty())->toArray();
    }

    protected function getAttributesArrayForClass(): array
    {
        return array_map(static fn (
            \ReflectionAttribute $attribute
        ): object => $attribute->newInstance(), $this->reflectionClass->getAttributes($this->attributeClass));
    }

    protected function getAttributesArrayForProperty(\ReflectionProperty $property): array
    {
        return array_map(static fn (\ReflectionAttribute $attribute): object => $attribute->newInstance(),
            $property->getAttributes($this->attributeClass));
    }

    protected function getPropertyCollection(string $propertyName, Collection $collection): Collection
    {
        $this->ensurePropertyCollectionHasPropertyName($propertyName, $collection);

        return $collection->get($propertyName);
    }

    /**
     * @return array<int, \ReflectionProperty>
     */
    protected function getPublicProperties(): array
    {
        return $this->reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);
    }

    protected function hasAnyPropertyCached(): bool
    {
        return $this->loaded['property'];
    }

    protected function hasClassLevelCached(): bool
    {
        return $this->loaded['class'];
    }

    protected function toCache(string $key, array $items): void
    {
        $this->loaded[$key] = true;
        $this->cached[$key] = $items;
    }
}
