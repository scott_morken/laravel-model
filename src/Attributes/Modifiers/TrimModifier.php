<?php

namespace Smorken\Model\Attributes\Modifiers;

use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Model;

class TrimModifier extends BaseModifier
{
    public function __construct(Mutation $type = Mutation::GET, protected string $characters = " \n\r\t\v\x00")
    {
        parent::__construct($type);
    }

    protected function modify(mixed $value, Model $model): mixed
    {
        return trim($value ?? '', $this->characters);
    }
}
