<?php

namespace Smorken\Model\Attributes\Modifiers;

use Carbon\Carbon;
use Smorken\Model\Contracts\Model;

class CarbonModifier extends BaseModifier
{
    protected function modify(mixed $value, Model $model): ?Carbon
    {
        if ($value) {
            if ($value instanceof \DateTime) {
                return Carbon::instance($value);
            }

            return Carbon::parse($value);
        }

        return null;
    }
}
