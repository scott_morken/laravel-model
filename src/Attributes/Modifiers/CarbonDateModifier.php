<?php

namespace Smorken\Model\Attributes\Modifiers;

use Carbon\Carbon;
use Smorken\Model\Contracts\Model;

class CarbonDateModifier extends BaseModifier
{
    protected function modify(mixed $value, Model $model): ?Carbon
    {
        if ($value) {
            if ($value instanceof \DateTime) {
                return Carbon::instance($value)->startOfDay();
            }

            return Carbon::parse($value)->startOfDay();
        }

        return null;
    }
}
