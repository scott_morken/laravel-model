<?php

namespace Smorken\Model\Attributes\Modifiers;

use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Attributes\Modifier;
use Smorken\Model\Contracts\Model;

abstract class BaseModifier implements Modifier
{
    public function __construct(protected Mutation $type = Mutation::GET) {}

    abstract protected function modify(mixed $value, Model $model): mixed;

    public function apply(mixed $value, Model $model, Mutation $type): mixed
    {
        if ($this->isMutation($type)) {
            return $this->modify($value, $model);
        }

        return $value;
    }

    public function isMutation(Mutation $type): bool
    {
        if ($this->type === Mutation::BOTH) {
            return true;
        }

        return $this->type === $type;
    }
}
