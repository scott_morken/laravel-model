<?php

namespace Smorken\Model\Attributes\Modifiers;

use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Model;

/**
 * This modifier can only be used inside a "prebuilt" Modifiers object.
 * Attributes do not appear to accept closures
 */
class ViaCallbackModifier extends BaseModifier
{
    public function __construct(protected \Closure $callback, Mutation $type = Mutation::GET)
    {
        parent::__construct($type);
    }

    public function __serialize(): array
    {
        return [
            'callback' => serialize($this->callback),
            'type' => $this->type,
        ];
    }

    public function __unserialize(array $data): void
    {
        $this->callback = unserialize($data['callback']);
        $this->type = $data['type'];
    }

    protected function modify(mixed $value, Model $model): mixed
    {
        return ($this->callback)($value, $model);
    }
}
