<?php

namespace Smorken\Model\Attributes\Modifiers;

use Smorken\Model\Contracts\Attributes\Modifier;
use Smorken\Model\Contracts\Model;

class BoolModifier extends BaseModifier implements Modifier
{
    protected function modify(mixed $value, Model $model): bool
    {
        if (is_bool($value)) {
            return $value;
        }
        if (is_string($value)) {
            return match (strtolower($value)) {
                'y', 'yes', '1', 'true' => true,
                'n', 'no', '0', 'false' => false,
                default => (bool) $value
            };
        }

        return (bool) $value;
    }
}
