<?php

namespace Smorken\Model\Attributes;

use Attribute;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Attributes\Modifier;
use Smorken\Model\Contracts\Model;

#[Attribute(Attribute::TARGET_PROPERTY)]
class ModifyValueAttribute
{
    public function __construct(protected Modifier $modifier) {}

    public function get(string $key, mixed $value, Model $model): mixed
    {
        return $this->apply($value, $model, Mutation::GET);
    }

    public function set(string $key, mixed $value, Model $model): mixed
    {
        return $this->apply($value, $model, Mutation::SET);
    }

    protected function apply(mixed $value, Model $model, Mutation $type): mixed
    {
        return $this->modifier->apply($value, $model, $type);
    }
}
