<?php

namespace Smorken\Model\Attributes;

use Attribute;
use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Model;

#[Attribute(Attribute::TARGET_CLASS)]
class ModifyValuesAttribute
{
    public function __construct(protected Modifiers $modifiers) {}

    public function get(string $key, mixed $value, Model $model): mixed
    {
        return $this->apply($key, $value, $model, Mutation::GET);
    }

    public function set(string $key, mixed $value, Model $model): mixed
    {
        return $this->apply($key, $value, $model, Mutation::SET);
    }

    protected function apply(string $key, mixed $value, Model $model, Mutation $type): mixed
    {
        /** @var \Smorken\Model\Contracts\Attributes\Modifier $modifier */
        foreach ($this->modifiers->for($key, $type) as $modifier) {
            $value = $modifier->apply($value, $model, $type);
        }

        return $value;
    }
}
