<?php

namespace Smorken\Model;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Concerns\HasAttributes;
use Smorken\Model\Concerns\WithFriendlyKeys;
use Smorken\Model\Contracts\Model;
use Smorken\Model\QueryBuilders\Builder;

#[\AllowDynamicProperties]
abstract class Eloquent extends \Illuminate\Database\Eloquent\Model implements Model
{
    use HasAttributes, WithFriendlyKeys;

    /** @use HasBuilder<Builder<static>> */
    use HasBuilder;

    /**
     * @var class-string<\Smorken\Model\QueryBuilders\Builder<*>>
     */
    protected static string $builder = Builder::class;

    public function __sleep()
    {
        $this->mergeAttributesFromCachedCasts();

        $this->classCastCache = [];
        $this->attributeCastCache = [];
        // set to empty arrays to avoid closure serialization
        if (property_exists($this, 'cacheRepository')) {
            $this->cacheRepository = null;
        }
        $this->attributesProviders = [];

        return array_keys(get_object_vars($this));
    }
}
