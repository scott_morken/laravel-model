<?php

namespace Smorken\Model\Contracts\Attributes;

interface Mapping
{
    public function desiredKeys(): array;

    public function for(string $key): string;

    public function has(string $key, bool $inverse = false): bool;

    public function inverse(string $key): string;
}
