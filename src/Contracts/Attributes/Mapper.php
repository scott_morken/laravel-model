<?php

declare(strict_types=1);

namespace Smorken\Model\Contracts\Attributes;

interface Mapper
{
    public function getMap(): array;

    public function inverse(string $key): string;

    public function map(string $key): string;
}
