<?php

namespace Smorken\Model\Contracts\Attributes;

interface AttributesProvider
{
    /**
     * @return array<int, \Attribute>
     */
    public function getClassLevelAttributes(): array;

    /**
     * @return array<int, \Attribute>
     */
    public function getPropertyLevelAttributes(string $propertyName): array;
}
