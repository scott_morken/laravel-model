<?php

namespace Smorken\Model\Contracts\Attributes;

use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Model;

interface Modifier
{
    public function isMutation(Mutation $type): bool;

    public function apply(mixed $value, Model $model, Mutation $type): mixed;
}
