<?php

namespace Smorken\Model\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Smorken\Model\Contracts\Concerns\HasAttributes;
use Smorken\Model\Contracts\Concerns\HasKey;

interface Model extends \ArrayAccess, \JsonSerializable, \Stringable, Arrayable, HasAttributes, HasKey, Jsonable {}
