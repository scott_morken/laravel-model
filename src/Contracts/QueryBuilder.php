<?php

namespace Smorken\Model\Contracts;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Support\Contracts\Filter;

/**
 * @phpstan-require-extends \Smorken\Model\QueryBuilders\Builder
 */
interface QueryBuilder extends Builder
{
    public function filter(Filter $filter): Builder;
}
