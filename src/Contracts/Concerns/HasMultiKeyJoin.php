<?php

namespace Smorken\Model\Contracts\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

interface HasMultiKeyJoin
{
    /**
     * @param  array<string, string>  $keys  foreign key => local key
     */
    public function multiKeyJoin(string $toTable, array $keys, string $joinType = 'join'): Builder;

    /**
     * @param  array<string, string>  $keys  foreign key => local key
     */
    public function scopeMultiKeyJoin(
        Builder $query,
        string $toTable,
        array $keys,
        string $joinType = 'join'
    ): Builder;
}
