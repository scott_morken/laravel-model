<?php

namespace Smorken\Model\Contracts\Concerns;

interface HasKey
{
    /**
     * @return mixed
     */
    public function getKey();

    public function getKeyName();
}
