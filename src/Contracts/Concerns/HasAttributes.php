<?php

namespace Smorken\Model\Contracts\Concerns;

interface HasAttributes
{
    public function attributesToArray();

    public function getAttribute($key);

    public function getAttributeValue($key);

    public function getAttributes();

    public function setAttribute($key, $value);

    public function setAttributes(iterable $attributes): void;
}
