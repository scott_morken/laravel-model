<?php

namespace Smorken\Model\Contracts\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder;

interface InvokableQueryScope
{
    public function __invoke(Builder $query): Builder;
}
