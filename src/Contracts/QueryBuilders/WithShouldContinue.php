<?php

namespace Smorken\Model\Contracts\QueryBuilders;

interface WithShouldContinue
{
    public function setShouldContinueClosure(\Closure $closure): void;
}
