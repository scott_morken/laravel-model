<?php

namespace Smorken\Model\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * Backwards compatibility
 */
trait WithDefaultScopes
{
    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query;
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }
}
