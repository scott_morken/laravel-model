<?php

namespace Smorken\Model\Concerns;

use Smorken\ArrayCache\Contracts\Repository;
use Smorken\Model\Attributes\Concerns\HasAttributesProviders;
use Smorken\Model\Attributes\ModifyValueAttribute;
use Smorken\Model\Attributes\ModifyValuesAttribute;

trait HasValueAttributeClassMutator
{
    use HasAttributesProviders;

    protected ?Repository $cacheRepository = null;

    protected array $dirtyKeys = [];

    protected function getCacheRepository(): Repository
    {
        if (! $this->cacheRepository) {
            $this->cacheRepository = \Smorken\ArrayCache\Repository::getInstance();
        }

        return $this->cacheRepository;
    }

    protected function getModifyValueByAttributes(string $key): array
    {
        if (! $this->keyIsDirty($key) && $this->getCacheRepository()->has([$this, $key])) {
            return $this->getCacheRepository()->get([$this, $key]) ?? [];
        }
        $this->dirtyKeys[$key] = false;
        $attributes = array_filter($this->getModifyValueByClassLevelAttributes() + $this->getModifyValueByPropertyLevelAttributes($key));
        $this->getCacheRepository()->put([$this, $key], $attributes);

        return $attributes;
    }

    protected function getModifyValueByClassLevelAttributes(): array
    {
        return $this->getAttributesProvider(ModifyValuesAttribute::class)->getClassLevelAttributes();
    }

    protected function getModifyValueByPropertyLevelAttributes(string $key): array
    {
        return $this->getAttributesProvider(ModifyValueAttribute::class)->getPropertyLevelAttributes($key);
    }

    protected function hasValueByAttributeClassMutator(string $key): bool
    {
        return (bool) $this->getModifyValueByAttributes($key);
    }

    protected function keyIsDirty(string $key): bool
    {
        return $this->dirtyKeys[$key] ?? false;
    }

    protected function mutateGetValueByAttributeClass(string $key, mixed $value): mixed
    {
        /** @var ModifyValuesAttribute $modifier */
        foreach ($this->getModifyValueByAttributes($key) as $modifier) {
            $value = $modifier->get($key, $value, $this);
        }

        return $value;
    }

    protected function mutateSetValueByAttributeClass(string $key, mixed $value): mixed
    {
        /** @var ModifyValuesAttribute $modifier */
        foreach ($this->getModifyValueByAttributes($key) as $modifier) {
            $value = $modifier->set($key, $value, $this);
        }
        $this->dirtyKeys[$key] = true;

        return $value;
    }
}
