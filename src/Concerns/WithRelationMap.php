<?php

namespace Smorken\Model\Concerns;

use Smorken\Model\Exceptions\RelationMapException;

/**
 * @property array<string, class-string> $relationMap
 */
trait WithRelationMap
{
    public function getRelationMap(): array
    {
        return property_exists($this, 'relationMap') ? $this->relationMap : [];
    }

    /**
     * @throws \Smorken\Model\Exceptions\RelationMapException
     */
    protected function getRelationClass(string $relation): string
    {
        return $this->getRelationMap()[$relation] ?? throw RelationMapException::notValid($relation);
    }
}
