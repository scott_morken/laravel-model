<?php

namespace Smorken\Model\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property array<string> $defaultColumns
 */
trait WithDefaultColumns
{
    protected bool $usePrefix = true;

    /**
     * If used in Builder context
     */
    public function defaultColumns(?string $prefix = null): Builder
    {
        return $this->scopeDefaultColumns($this, $prefix);
    }

    /**
     * If used in Model context
     */
    public function scopeDefaultColumns(Builder $query, ?string $prefix = null): Builder
    {
        $columns = $this->getDefaultColumns($prefix);

        return $columns ? $query->select($columns) : $query;
    }

    public function setUseDefaultColumnPrefix(bool $usePrefix): self
    {
        $this->usePrefix = $usePrefix;

        return $this;
    }

    protected function getDefaultColumnPrefix(?string $prefix): ?string
    {
        if ($this->shouldUseDefaultColumnPrefix()) {
            return $prefix ?? $this->getTable();
        }

        return null;
    }

    protected function getDefaultColumns(?string $prefix = null): array|false
    {
        $defaultColumns = property_exists($this, 'defaultColumns') ? $this->defaultColumns : [];
        if (! empty($defaultColumns)) {
            $prefix = $this->getDefaultColumnPrefix($prefix);

            return array_map(static fn (string $column): string => $prefix ? $prefix.'.'.$column : $column,
                $defaultColumns);
        }

        return false;
    }

    protected function shouldUseDefaultColumnPrefix(): bool
    {
        return $this->usePrefix;
    }
}
