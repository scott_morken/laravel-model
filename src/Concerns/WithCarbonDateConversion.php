<?php

namespace Smorken\Model\Concerns;

use Carbon\Carbon;

trait WithCarbonDateConversion
{
    protected function toCarbonDate(string $attr): ?Carbon
    {
        $value = $this->attributes[$attr] ?? null;

        return $this->valueToCarbonDate($value);
    }

    protected function valueToCarbonDate(string|Carbon|null $value): ?Carbon
    {
        if (method_exists($this, 'asDateTime')) {
            return $this->asDateTime($value);
        }
        if ($value) {
            if ($value instanceof \DateTime) {
                return Carbon::instance($value);
            }

            return Carbon::parse($value);
        }

        return null;
    }
}
