<?php

declare(strict_types=1);

namespace Smorken\Model\Concerns;

use Smorken\Model\Attributes\Concerns\HasAttributesProviders;
use Smorken\Model\Attributes\MapAttributeNames;

trait HasAttributeNamesAttributeClass
{
    use HasAttributesProviders;

    protected bool $initializedMapAttributeNames = false;

    protected array $mapAttributeNamesAttributes = [];

    protected bool $skipAttributeNameMapping = false;

    public function skipAttributeNameMapping(bool $skip = true): self
    {
        $this->skipAttributeNameMapping = $skip;

        return $this;
    }

    protected function getInverseMapAttributeNamesFor(string $key): string
    {
        if ($this->skipAttributeNameMapping) {
            return $key;
        }
        /** @var MapAttributeNames $attribute */
        foreach ($this->getMapAttributeNamesClassLevelAttributes() as $attribute) {
            $key = $attribute->inverse($key);
        }

        return $key;
    }

    protected function getMapAttributeNamesClassLevelAttributes(): array
    {
        if (empty($this->mapAttributeNamesAttributes) && ! $this->initializedMapAttributeNames) {
            $this->mapAttributeNamesAttributes = array_filter($this->getAttributesProvider(MapAttributeNames::class)
                ->getClassLevelAttributes());
            $this->initializedMapAttributeNames = true;
        }

        return $this->mapAttributeNamesAttributes;
    }

    protected function getMapAttributeNamesFor(string $key): string
    {
        if ($this->skipAttributeNameMapping) {
            return $key;
        }
        /** @var MapAttributeNames $attribute */
        foreach ($this->getMapAttributeNamesClassLevelAttributes() as $attribute) {
            $key = $attribute->map($key);
        }

        return $key;
    }

    protected function mapAttributeNames(array $attributes): array
    {
        $mapped = [];
        foreach ($attributes as $key => $value) {
            $mapped[$this->getMapAttributeNamesFor($key)] = $value;
        }

        return $this->applyMapToMapped($mapped);
    }

    protected function mapInverseAttributeNames(array $attributes): array
    {
        if ($this->skipAttributeNameMapping) {
            return $attributes;
        }
        $mapped = [];
        foreach ($attributes as $key => $value) {
            $mapped[$this->getInverseMapAttributeNamesFor($key)] = $value;
        }

        return $this->applyMapToMapped($mapped);
    }

    protected function applyMapToMapped(array $mapped): array
    {
        foreach ($this->getMapAttributeNamesClassLevelAttributes() as $attribute) {
            $map = $attribute->getMap();
            foreach ($map as $newAtt => $originalAtt) {
                $mapped[$newAtt] = $this->getAttribute($originalAtt);
            }
        }

        return $mapped;
    }
}
