<?php

namespace Smorken\Model\Concerns;

trait HasAttributes
{
    use HasAttributeNamesAttributeClass,
        HasValueAttributeClassMutator,
        \Illuminate\Database\Eloquent\Concerns\HasAttributes {
            \Illuminate\Database\Eloquent\Concerns\HasAttributes::attributesToArray as parentAttributesToArray;
            \Illuminate\Database\Eloquent\Concerns\HasAttributes::setAttribute as parentSetAttribute;
            \Illuminate\Database\Eloquent\Concerns\HasAttributes::transformModelValue as parentTransformModelValue;
        }

    protected array $addAttributeMutatedAttributes = [];

    public function attributesToArray()
    {
        $attributes = $this->parentAttributesToArray();
        $attributes = $this->addAttributesViaAttributeMutators($attributes);

        return $this->mapInverseAttributeNames($attributes);
    }

    public function getAttribute($key)
    {
        if (! $key) {
            return;
        }

        if (! $this->hasGetMutator($key) && ! $this->hasAttributeMutator($key)) {
            $key = $this->getMapAttributeNamesFor($key);
        }

        // If the attribute exists in the attribute array or has a "get" mutator we will
        // get the attribute's value. Otherwise, we will proceed as if the developers
        // are asking for a relationship's value. This covers both types of values.
        if ($this->wantsToGetAttributeValue($key)) {
            return $this->getAttributeValue($key);
        }

        // Here we will determine if the model base class itself contains this given key
        // since we don't want to treat any of those methods as relationships because
        // they are all intended as helper methods and none of these are relations.
        if (method_exists(self::class, $key)) {
            return $this->throwMissingAttributeExceptionIfApplicable($key);
        }

        return $this->isRelation($key) || $this->relationLoaded($key)
            ? $this->getRelationValue($key)
            : $this->throwMissingAttributeExceptionIfApplicable($key);
    }

    public function hasAttribute($key)
    {
        if (method_exists(parent::class, 'hasAttribute')) {
            return parent::hasAttribute($key);
        }

        return $this->internalHasAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        $key = $this->getMapAttributeNamesFor($key);

        if ($this->hasValueByAttributeClassMutator($key)) {
            $value = $this->mutateSetValueByAttributeClass($key, $value);
        }

        return $this->parentSetAttribute($key, $value);
    }

    public function setAttributes(iterable $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    protected function addAttributesViaAttributeMutators(array $attributes): array
    {
        foreach ($attributes as $key => $value) {
            if ($this->hasValueByAttributeClassMutator($key)) {
                $attributes[$key] = $this->mutateGetValueByAttributeClass($key, $value);
            }
        }
        foreach ($this->addAttributeMutatedAttributes as $key) {
            $attributes[$key] = $this->mutateGetValueByAttributeClass($key, null);
        }

        return $attributes;
    }

    protected function internalHasAttribute($key): bool
    {
        if (! $key) {
            return false;
        }

        return array_key_exists($key, $this->attributes) ||
            array_key_exists($key, $this->casts) ||
            $this->hasGetMutator($key) ||
            $this->hasAttributeMutator($key) ||
            $this->isClassCastable($key);
    }

    protected function transformModelValue($key, $value): mixed
    {
        $value = $this->parentTransformModelValue($key, $value);
        if ($this->hasValueByAttributeClassMutator($key)) {
            $value = $this->mutateGetValueByAttributeClass($key, $value);
        }

        return $value;
    }

    protected function wantsToGetAttributeValue($key): bool
    {
        return $this->hasAttribute($key) || in_array($key, $this->addAttributeMutatedAttributes, true);
    }
}
