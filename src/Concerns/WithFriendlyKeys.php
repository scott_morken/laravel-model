<?php

namespace Smorken\Model\Concerns;

use Smorken\Support\Str;

/**
 * @property array<string, string> $friendlyKeys
 */
trait WithFriendlyKeys
{
    public function friendlyKey(string $key): string
    {
        $friendlyKey = $this->getFriendlyKeys()[$key] ?? Str::friendlyName($key);
        if ($friendlyKey instanceof \Closure) {
            return $friendlyKey($key);
        }

        return $friendlyKey;
    }

    protected function getFriendlyKeys(): array
    {
        return property_exists($this, 'friendlyKeys') ? $this->friendlyKeys : [];
    }
}
