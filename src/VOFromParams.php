<?php

namespace Smorken\Model;

use Illuminate\Support\Collection;
use Smorken\Model\Concerns\HasValueAttributeClassMutator;

abstract class VOFromParams extends BaseVO
{
    use HasValueAttributeClassMutator;

    protected string|array|null $keyName = 'id';

    public function __construct()
    {
        $this->applySetOnPublicAttributes();
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function __sleep()
    {
        // set to empty arrays to avoid closure serialization
        if (property_exists($this, 'cacheRepository')) {
            $this->cacheRepository = null;
        }
        $this->attributesProviders = [];

        return array_keys(get_object_vars($this));
    }

    public function attributesToArray(): array
    {
        return $this->getAttributes();
    }

    public function getAttribute($key)
    {
        if (! $key) {
            return null;
        }

        return $this->getAttributeValue($key);
    }

    public function getAttributeValue($key)
    {
        if ($this->offsetExists($key)) {
            return $this->transformModelValue($key, $this->$key);
        }

        return null;
    }

    public function getAttributes(): array
    {
        return [...$this->getPublicProperties()];
    }

    public function newInstance(array $attributes = []): static
    {
        // @phpstan-ignore new.static
        return new static(...$attributes);
    }

    public function offsetExists(mixed $offset): bool
    {
        return property_exists($this, $offset);
    }

    public function offsetUnset(mixed $offset): void
    {
        if ($this->offsetExists($offset)) {
            $this->setAttribute($offset, null);
        }
    }

    public function setAttribute($key, $value): void
    {
        if (! $key) {
            return;
        }
        $this->setAttributeValue($key, $value);
    }

    public function setAttributes(iterable $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    protected function applySetOnPublicAttributes(): void
    {
        foreach ($this->getRawPublicProperties() as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * @return array<string, mixed>
     */
    protected function getPublicProperties(): array
    {
        $collectedProperties = new Collection(
            $this->getRawPublicProperties()
        );

        return $collectedProperties
            ->mapWithKeys(fn (
                mixed $value,
                string $key
            ) => [$key => $this->getAttribute($key)])
            ->toArray();
    }

    /**
     * @return array<string, mixed>
     */
    protected function getRawPublicProperties(): array
    {
        $collected = new Collection((new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC));

        return $collected
            ->mapWithKeys(fn (\ReflectionProperty $property) => [$property->getName() => $this->{$property->getName()}])
            ->toArray();
    }

    protected function setAttributeValue(string $key, mixed $value): void
    {
        if ($this->offsetExists($key)) {
            if ($this->hasValueByAttributeClassMutator($key)) {
                $value = $this->mutateSetValueByAttributeClass($key, $value);
            }
            $this->$key = $value;
        }
    }

    protected function transformModelValue(string $key, mixed $value): mixed
    {
        if ($this->hasValueByAttributeClassMutator($key)) {
            $value = $this->mutateGetValueByAttributeClass($key, $value);
        }

        return $value;
    }
}
