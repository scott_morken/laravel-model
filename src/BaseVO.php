<?php

namespace Smorken\Model;

use Illuminate\Database\Eloquent\Concerns\HidesAttributes;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Str;
use Smorken\Model\Concerns\WithFriendlyKeys;
use Smorken\Model\Contracts\Model;

abstract class BaseVO implements Model
{
    use HidesAttributes, WithFriendlyKeys;

    protected bool $escapeWhenCastingToString = false;

    protected bool $exists = false;

    /**
     * @var array<string, string>
     */
    protected array $friendlyKeys = [];

    protected string|array|null $keyName = 'id';

    protected array $relations = [];

    protected ?string $table = null;

    protected bool $wasRecentlyCreated = true;

    public function __toString(): string
    {
        return $this->escapeWhenCastingToString
            ? e($this->toJson())
            : $this->toJson();
    }

    public function fill(array $attributes): self
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }

        return $this;
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKey(): mixed
    {
        $keyName = $this->getKeyName();
        if ($keyName === null) {
            return null;
        }
        if (is_array($keyName)) {
            return array_map(fn ($key) => $this->getAttribute($key), $keyName);
        }

        return $this->getAttribute($keyName);
    }

    public function getKeyName(): string|array|null
    {
        return $this->keyName;
    }

    public function getTable(): string
    {
        return $this->table ?? Str::snake(Str::pluralStudly(class_basename($this)));
    }

    public function setTable(string $table): self
    {
        $this->table = $table;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function newInstance(array $attributes = []): static
    {
        // @phpstan-ignore new.static
        return new static($attributes);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->getAttribute($offset);
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->setAttribute($offset, $value);
    }

    public function qualifyColumn(string $column): string
    {
        if (str_contains($column, '.')) {
            return $column;
        }

        return $this->getTable().'.'.$column;
    }

    public function toArray(): array
    {
        return $this->attributesToArray();
    }

    public function toJson($options = 0): string
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    public function usesTimestamps(): bool
    {
        return false;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value);
    }
}
