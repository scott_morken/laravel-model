<?php

namespace Smorken\Model\Exceptions;

final class RelationMapException extends \Exception
{
    public static function notValid(string $relation): static
    {
        return new self("{$relation} is not a valid relation map relation.");
    }
}
