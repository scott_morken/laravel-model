<?php

namespace Smorken\Model\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Contracts\QueryBuilders\WithShouldContinue;

class InvokableQueryScope implements \Smorken\Model\Contracts\QueryBuilders\InvokableQueryScope, WithShouldContinue
{
    use \Smorken\Model\QueryBuilders\Concerns\WithShouldContinue;

    protected mixed $value;

    public function __construct(protected string|\Closure $method, mixed $value)
    {
        $this->value = $this->ensureValue($value);
    }

    /**
     * @template TBuilder of Builder
     *
     * @param  TBuilder  $query
     * @return TBuilder
     */
    public function __invoke(Builder $query): Builder
    {
        if (! $this->shouldContinue($this->value)) {
            return $query;
        }
        if ($this->method instanceof \Closure) {
            return ($this->method)($query, $this->value);
        }

        return $query->{$this->method}($this->value);
    }

    protected function ensureValue(mixed $value): mixed
    {
        return $value;
    }
}
