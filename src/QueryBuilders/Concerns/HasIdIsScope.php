<?php

namespace Smorken\Model\QueryBuilders\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

trait HasIdIsScope
{
    public function idIs(string|int|array $id): Builder
    {
        return $this->scopeIdIs($this, $id);
    }

    public function scopeIdIs(Builder $query, string|int|array $id): Builder
    {
        if (is_array($id)) {
            return $this->keysMatchIdArray($query, $id);
        }

        return $query->where($query->model->getKeyName(), '=', $id);
    }

    protected function keysMatchIdArray(Builder $query, array $id): Builder
    {
        return $query->where(static function (Builder $q) use ($id) {
            foreach ($id as $k => $v) {
                $q->where($k, '=', $v);
            }
        });
    }
}
