<?php

namespace Smorken\Model\QueryBuilders\Concerns;

trait WithShouldContinue
{
    protected ?\Closure $shouldContinueClosure = null;

    public function setShouldContinueClosure(\Closure $closure): void
    {
        $this->shouldContinueClosure = $closure;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function shouldContinue(mixed $value): bool
    {
        if ($this->shouldContinueClosure) {
            return ($this->shouldContinueClosure)($value);
        }

        return $this->shouldContinueByValue($value);
    }

    protected function shouldContinueByValue(mixed $value): bool
    {
        return ! $this->isEmpty($value);
    }
}
