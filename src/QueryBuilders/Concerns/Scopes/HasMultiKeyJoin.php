<?php

namespace Smorken\Model\QueryBuilders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;

trait HasMultiKeyJoin
{
    public function multiKeyJoin(string $toTable, array $keys, string $joinType = 'join'): Builder
    {
        return $this->scopeMultiKeyJoin($this, $toTable, $keys, $joinType);
    }

    public function scopeMultiKeyJoin(Builder $query, string $toTable, array $keys, string $joinType = 'join'): Builder
    {
        $keys = $this->qualifyCompositeKeys($toTable, $this->getFromTableName(), $keys);
        $on = [];
        foreach ($keys as $local => $foreign) {
            $on[] = [$local, '=', $foreign];
        }
        $query->$joinType(
            $toTable,
            function ($join) use ($on) {
                foreach ($on as $j) {
                    call_user_func_array([$join, 'on'], $j);
                }
            }
        );

        return $query;
    }

    protected function getFromTableName(): string
    {
        if ($this instanceof Builder) {
            return $this->model->getTable();
        }

        return $this->getTable();
    }

    protected function qualifyCompositeKeys(
        string $toTable,
        string $fromTable,
        array $keys,
        bool $addTable = true
    ): array {
        $qualified = [];
        foreach ($keys as $foreign => $local) {
            if (is_int($foreign)) {
                $foreign = $local;
            }
            $f = $addTable ? $toTable.'.'.$foreign : $foreign;
            $l = $addTable ? $fromTable.'.'.$local : $local;
            $qualified[$f] = $l;
        }

        return $qualified;
    }
}
