<?php

namespace Smorken\Model\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Smorken\Model\Contracts\QueryBuilder;
use Smorken\Model\Filters\Filterable;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of \Illuminate\Database\Eloquent\Model
 *
 * @property-read HigherOrderBuilderProxy $orWhere
 * @property-read HigherOrderBuilderProxy $whereNot
 * @property-read HigherOrderBuilderProxy $orWhereNot
 *
 * @mixin \Illuminate\Database\Query\Builder
 */
class Builder extends \Illuminate\Database\Eloquent\Builder implements QueryBuilder
{
    /**
     * @return $this
     */
    public function defaultOrder(): self|EloquentBuilder
    {
        if ($this->hasNamedScope(__FUNCTION__)) {
            return $this->callNamedScope(__FUNCTION__);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function defaultWiths(): self|EloquentBuilder
    {
        if ($this->hasNamedScope(__FUNCTION__)) {
            return $this->callNamedScope(__FUNCTION__);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function filter(Filter $filter): self|EloquentBuilder
    {
        $invokable = new ApplyFilterableToQuery($this->getFilterable($filter));

        return $invokable->apply($this);
    }

    /**
     * @return iterable<\Smorken\Model\Filters\FilterHandler>
     */
    protected function getFilterHandlers(): iterable
    {
        return [];
    }

    protected function getFilterable(Filter $filter): Filterable
    {
        return new Filterable($filter, $this->getFilterHandlers());
    }
}
