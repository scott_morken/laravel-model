<?php

namespace Smorken\Model\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\Filters\Filterable;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Support\Contracts\Filter;

class ApplyFilterableToQuery
{
    public function __construct(protected Filterable $filterable) {}

    /**
     * @template TBuilder of EloquentBuilder
     *
     * @param  TBuilder  $builder
     * @return TBuilder
     */
    public function apply(EloquentBuilder $builder): EloquentBuilder
    {
        foreach ($this->filterable->handlers as $handler) {
            $invokable = $this->createInvokableQueryBuilder($handler, $this->filterable->filter);
            $builder = $invokable($builder);
        }

        return $builder;
    }

    protected function createInvokableQueryBuilder(
        FilterHandler $handler,
        Filter $filter
    ): \Smorken\Model\Contracts\QueryBuilders\InvokableQueryScope {
        $handlerClass = $handler->builderClass;
        $value = $filter->getAttribute($handler->filterKey);

        return new $handlerClass($handler->queryMethod, $value);
    }
}
