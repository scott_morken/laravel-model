<?php

namespace Tests\Smorken\Model\Unit;

use Smorken\Support\Filter;
use Tests\Smorken\Model\Stubs\Models\ModelWithBuilderStub;

class ModelWithFiltersTest extends TestCaseWithMockConnectionResolver
{
    public function testAppliesFilters(): void
    {
        $sut = (new ModelWithBuilderStub)->newQuery();
        $filter = new Filter([
            'f_active' => 1,
            'f_bars' => ['b1', 'b2'],
            'f_fooBar' => 'fb',
        ]);
        $query = $sut->filter($filter);
        $this->assertEquals('select * from `model_with_builder_stubs` where `foo` = ?',
            $query->toSql());
    }
}
