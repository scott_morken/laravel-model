<?php

namespace Tests\Smorken\Model\Unit;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Concerns\WithMockConnection;

class TestCaseWithMockConnectionResolver extends TestCase
{
    use WithMockConnection;

    protected function setUp(): void
    {
        parent::setUp();
        $cr = new ConnectionResolver(['db' => $this->getMockConnection(MySqlConnection::class)]);
        $cr->setDefaultConnection('db');
        Model::setConnectionResolver($cr);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
