<?php

namespace Tests\Smorken\Model\Unit;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Stubs\Models\ModelWithVirtualAttributeMutatorAttributeStub;

class ModelWithVirtualAttributeMutatorAttributeTest extends TestCase
{
    public function testVirtualAttributeHasValue(): void
    {
        $sut = (new ModelWithVirtualAttributeMutatorAttributeStub)->forceFill(['bar' => 'not foo']);
        $this->assertEquals('modified', $sut->foo);
    }

    public function testVirtualAttributeIsAdded(): void
    {
        $sut = (new ModelWithVirtualAttributeMutatorAttributeStub)->forceFill(['bar' => 'not foo']);
        $this->assertEquals([
            'bar' => 'not foo',
            'foo' => 'modified',
        ], $sut->toArray());
    }

    public function testVirtualAttributeSerialized(): void
    {
        $sut = (new ModelWithVirtualAttributeMutatorAttributeStub)->forceFill(['bar' => 'not foo']);
        $s = serialize($sut);
        $this->assertEquals([
            'bar' => 'not foo',
            'foo' => 'modified',
        ], unserialize($s)->toArray());
    }
}
