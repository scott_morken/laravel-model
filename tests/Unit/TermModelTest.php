<?php

namespace Tests\Smorken\Model\Unit;

use Carbon\Carbon;
use Tests\Smorken\Model\Stubs\Models\TermModelStub;

class TermModelTest extends TestCaseWithMockConnectionResolver
{
    public function testCanSerialize(): void
    {
        $sut = (new TermModelStub)->forceFill([
            'STRM' => 'T1234',
            'TERM_BEGIN_DT' => '2022-01-01',
            'TERM_END_DT' => '2022-05-01',
            'test' => 'test',
        ]);
        $s = serialize($sut);
        $sut2 = unserialize($s);
        $this->assertInstanceOf(Carbon::class, $sut2->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $sut2->TERM_END_DT);
        $this->assertEquals('foo bartest', $sut2->test);
    }

    public function testValueModifiers(): void
    {
        $sut = (new TermModelStub)->forceFill([
            'STRM' => 'T1234',
            'TERM_BEGIN_DT' => '2022-01-01',
            'TERM_END_DT' => '2022-05-01',
            'test' => 'test',
        ]);
        $this->assertInstanceOf(Carbon::class, $sut->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $sut->TERM_END_DT);
        $this->assertEquals('foo bartest', $sut->test);
    }
}
