<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Unit\Attributes;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\Mappers\MapToArray;
use Smorken\Model\Attributes\Mappers\MapToCamelCase;
use Smorken\Model\Attributes\Mappers\MapToSnakeCase;
use Smorken\Model\Eloquent;

class MapAttributeNamesTest extends TestCase
{
    #[Test]
    public function it_does_not_map_toArray_using_snake_case_mapper_when_no_context_is_created(): void
    {
        $sut = new #[MapAttributeNames(new MapToSnakeCase)] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $this->assertEquals([
            'foo_bar' => 'foo',
            'fiz_buz' => 'fiz',
        ], $sut->toArray());
    }

    #[Test]
    public function it_maps_an_attribute_name_using_array_mapper(): void
    {
        $sut = new #[MapAttributeNames(new MapToArray(['other_foo' => 'foo_bar', 'other_fiz' => 'fiz_buz']))] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $this->assertEquals('foo', $sut->getAttribute('other_foo'));
        $this->assertEquals('foo', $sut->other_foo);
        $this->assertEquals('fiz', $sut->getAttribute('other_fiz'));
        $this->assertEquals('fiz', $sut->other_fiz);
        $this->assertEquals([
            'other_foo' => 'foo',
            'other_fiz' => 'fiz',
        ], $sut->toArray());
    }

    #[Test]
    public function it_does_not_an_attribute_name_when_get_mutator_exists(): void
    {
        $sut = new #[MapAttributeNames(new MapToArray(['other_foo' => 'foo_bar', 'other_fiz' => 'fiz_buz']))] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];

            public function getOtherFooAttribute(): ?string
            {
                return $this->attributes['fiz_buz'];
            }
        };
        $this->assertEquals('fiz', $sut->getAttribute('other_foo'));
        $this->assertEquals('fiz', $sut->other_foo);
    }

    #[Test]
    public function it_maps_duplicate_attribute_names_using_array_mapper(): void
    {
        $sut = new #[MapAttributeNames(new MapToArray(['other_foo' => 'foo_bar', 'other_fiz' => 'fiz_buz', 'other_foo_2' => 'foo_bar']))] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $this->assertEquals('foo', $sut->getAttribute('other_foo'));
        $this->assertEquals('foo', $sut->other_foo);
        $this->assertEquals('foo', $sut->getAttribute('other_foo_2'));
        $this->assertEquals('foo', $sut->other_foo_2);
        $this->assertEquals('fiz', $sut->getAttribute('other_fiz'));
        $this->assertEquals('fiz', $sut->other_fiz);
        $this->assertEquals([
            'other_foo' => 'foo',
            'other_fiz' => 'fiz',
            'other_foo_2' => 'foo',
        ], $sut->toArray());
    }

    #[Test]
    public function it_maps_an_attribute_name_using_camel_case_mapper(): void
    {
        $sut = new #[MapAttributeNames(new MapToCamelCase)] class(['fooBar' => 'foo', 'fizBuz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['fooBar', 'fizBuz'];
        };
        $this->assertEquals('foo', $sut->getAttribute('foo_bar'));
        $this->assertEquals('foo', $sut->foo_bar);
        $this->assertEquals('fiz', $sut->getAttribute('fiz_buz'));
        $this->assertEquals('fiz', $sut->fiz_buz);
        $this->assertEquals([
            'foo_bar' => 'foo',
            'fiz_buz' => 'fiz',
        ], $sut->toArray());
    }

    #[Test]
    public function it_maps_an_attribute_name_using_snake_case_mapper(): void
    {
        $sut = new #[MapAttributeNames(new MapToSnakeCase)] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $this->assertEquals('foo', $sut->getAttribute('fooBar'));
        $this->assertEquals('foo', $sut->fooBar);
        $this->assertEquals('fiz', $sut->getAttribute('fizBuz'));
        $this->assertEquals('fiz', $sut->fizBuz);
        $this->assertEquals([
            'fooBar' => 'foo',
            'fizBuz' => 'fiz',
        ], $sut->toArray());
    }

    #[Test]
    public function it_sets_an_attribute_using_the_map(): void
    {
        $sut = new #[MapAttributeNames(new MapToArray(['other_foo' => 'foo_bar', 'other_fiz' => 'fiz_buz']))] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $this->assertEquals('foo', $sut->getAttribute('other_foo'));
        $this->assertEquals('foo', $sut->other_foo);
        $sut->other_fiz = 'buz';
        $this->assertEquals([
            'other_foo' => 'foo',
            'other_fiz' => 'buz',
        ], $sut->toArray());
        $this->assertEquals('buz', $sut->getAttribute('fiz_buz'));
    }

    #[Test]
    public function it_skips_mapping_attribute_names_using_array_mapper(): void
    {
        $sut = new #[MapAttributeNames(new MapToArray(['other_foo' => 'foo_bar', 'other_fiz' => 'fiz_buz']))] class(['foo_bar' => 'foo', 'fiz_buz' => 'fiz']) extends Eloquent
        {
            protected $fillable = ['foo_bar', 'fiz_buz'];
        };
        $sut->skipAttributeNameMapping();
        $this->assertEquals(null, $sut->getAttribute('other_foo'));
        $this->assertEquals(null, $sut->other_foo);
        $this->assertEquals(null, $sut->getAttribute('other_fiz'));
        $this->assertEquals(null, $sut->other_fiz);
        $this->assertEquals([
            'foo_bar' => 'foo',
            'fiz_buz' => 'fiz',
        ], $sut->toArray());
    }
}
