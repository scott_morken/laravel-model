<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Unit\Attributes\Mappers;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Mappers\MapToSnakeCase;

class MapToSnakeCaseTest extends TestCase
{
    #[Test]
    public function it_uses_original_key_if_it_exists_for_inverse(): void
    {
        $sut = new MapToSnakeCase;
        $this->assertEquals('original_key', $sut->map('OriginalKey'));
        $this->assertEquals('OriginalKey', $sut->inverse('original_key'));
    }

    #[Test]
    public function it_returns_the_provided_key_if_no_original_key_exists_for_inverse(): void
    {
        $sut = new MapToSnakeCase;
        $this->assertEquals('original_key', $sut->inverse('original_key'));
    }
}
