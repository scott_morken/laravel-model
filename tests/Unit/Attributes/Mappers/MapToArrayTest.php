<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Unit\Attributes\Mappers;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Mappers\MapToArray;

class MapToArrayTest extends TestCase
{
    #[Test]
    public function it_inverses_the_map(): void
    {
        $sut = new MapToArray([
            'fooBar' => 'foo_bar',
            'fizBuz' => 'fiz_buz',
        ]);
        $this->assertEquals('fooBar', $sut->inverse('foo_bar'));
        $this->assertEquals('fizBuz', $sut->inverse('fiz_buz'));
        $this->assertEquals('bar_baz', $sut->inverse('bar_baz'));
    }

    #[Test]
    public function it_maps_the_keys(): void
    {
        $sut = new MapToArray([
            'fooBar' => 'foo_bar',
            'fizBuz' => 'fiz_buz',
        ]);
        $this->assertEquals('foo_bar', $sut->map('fooBar'));
        $this->assertEquals('fiz_buz', $sut->map('fizBuz'));
        $this->assertEquals('barBaz', $sut->map('barBaz'));
    }
}
