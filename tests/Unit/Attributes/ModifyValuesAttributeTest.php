<?php

namespace Tests\Smorken\Model\Unit\Attributes;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Stubs\Models\ModelWithArrayModifiersStub;
use Tests\Smorken\Model\Stubs\Models\VOFromParamsWithModifiersStub;

class ModifyValuesAttributeTest extends TestCase
{
    public function testWithArrayForGetters(): void
    {
        $sut = new ModelWithArrayModifiersStub(['foo' => '   hi   ', 'bar' => null]);
        $this->assertEquals('hi', $sut->foo);
        $this->assertNull($sut->bar);
    }

    public function testWithModifyPropertyForSetters(): void
    {
        $sut = new VOFromParamsWithModifiersStub(
            1,
            '2022-01-01',
            '    text   ',
        );
        $this->assertEquals(1, $sut->id);
        $this->assertInstanceOf(Carbon::class, $sut->today);
        $this->assertEquals('text', $sut->someText);
    }
}
