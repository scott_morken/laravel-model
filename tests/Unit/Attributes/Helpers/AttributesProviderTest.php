<?php

namespace Tests\Smorken\Model\Unit\Attributes\Helpers;

use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Helpers\AttributesProvider;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Model\Attributes\ModifyValueAttribute;
use Smorken\Model\VOFromParams;

class AttributesProviderTest extends TestCase
{
    public function testGetPropertyLevelAttributesExistWithSameAttributeType(): void
    {
        $model = new class(99, ' foo ') extends VOFromParams
        {
            public function __construct(
                public int $id,
                #[ModifyValueAttribute(new TrimModifier)]
                public string $text
            ) {
                parent::__construct();
            }
        };
        $sut = new AttributesProvider(ModifyValueAttribute::class, $model);
        $this->assertEmpty($sut->getPropertyLevelAttributes('id'));
        $this->assertCount(1, $sut->getPropertyLevelAttributes('text'));
    }
}
