<?php

namespace Tests\Smorken\Model\Unit\Attributes\Modifiers;

use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\VO;

class TrimModifierTest extends TestCase
{
    public function testNull(): void
    {
        $sut = new TrimModifier;
        $m = new VO;
        $this->assertEquals('', $sut->apply(null, $m, Mutation::GET));
    }

    public function testTrimsOverriddenCharacters(): void
    {
        $sut = new TrimModifier(Mutation::GET, '\\');
        $m = new VO;
        $this->assertEquals('App\\Test\\Blah', $sut->apply('\\App\\Test\\Blah\\', $m, Mutation::GET));
    }

    public function testTrimsStandard(): void
    {
        $sut = new TrimModifier;
        $m = new VO;
        $tests = [' foo ', "\nfoo\n", "\rfoo\r", "\tfoo\t", "\vfoo\v", "\x00foo\x00"];
        foreach ($tests as $i => $test) {
            $this->assertEquals('foo', $sut->apply($test, $m, Mutation::GET), "Test index {$i}");
        }
    }
}
