<?php

namespace Tests\Smorken\Model\Unit\Attributes\Modifiers;

use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Modifiers\ViaCallbackModifier;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\Contracts\Model;
use Smorken\Model\VO;

class ViaCallbackModifierTest extends TestCase
{
    public function testCallbackApplied(): void
    {
        $sut = new ViaCallbackModifier(static fn (mixed $value, Model $model) => "{$model->id} {$value}");
        $m = new VO(['id' => 'foo']);
        $this->assertEquals('foo bar', $sut->apply('bar', $m, Mutation::GET));
    }
}
