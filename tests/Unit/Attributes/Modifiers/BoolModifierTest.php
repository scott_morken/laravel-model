<?php

namespace Tests\Smorken\Model\Unit\Attributes\Modifiers;

use PHPUnit\Framework\TestCase;
use Smorken\Model\Attributes\Modifiers\BoolModifier;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\VO;

class BoolModifierTest extends TestCase
{
    public function testBooleans(): void
    {
        $sut = new BoolModifier;
        $m = new VO;
        $this->assertTrue($sut->apply(true, $m, Mutation::GET));
        $this->assertFalse($sut->apply(false, $m, Mutation::GET));
    }

    public function testFalsey(): void
    {
        $sut = new BoolModifier;
        $m = new VO;
        $falsey = [0, '', null];
        foreach ($falsey as $v) {
            $this->assertFalse($sut->apply($v, $m, Mutation::GET), "Testing {$v}");
        }
    }

    public function testStrings(): void
    {
        $sut = new BoolModifier;
        $m = new VO;
        $truthy = ['y', 'Y', 'YES', 'yes', '1', 'true', 'TRUE'];
        foreach ($truthy as $v) {
            $this->assertTrue($sut->apply($v, $m, Mutation::GET), "Testing {$v}");
        }
        $falsey = ['n', 'N', 'NO', 'no', '0', 'false', 'FALSE'];
        foreach ($falsey as $v) {
            $this->assertFalse($sut->apply($v, $m, Mutation::GET), "Testing {$v}");
        }
    }

    public function testTruthy(): void
    {
        $sut = new BoolModifier;
        $m = new VO;
        $truthy = [1, 1000, -100, new \stdClass];
        foreach ($truthy as $i => $v) {
            $this->assertTrue($sut->apply($v, $m, Mutation::GET), "Testing index {$i}");
        }
    }
}
