<?php

namespace Tests\Smorken\Model\Unit\Attributes\Modifiers;

use PHPUnit\Framework\TestCase;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\VO;
use Tests\Smorken\Model\Stubs\Modifiers\ModifierStub;

class BaseModifierTest extends TestCase
{
    public function testBothMutation(): void
    {
        $sut = new ModifierStub(Mutation::BOTH);
        $m = new VO;
        $this->assertEquals('modified', $sut->apply('foo', $m, Mutation::GET));
        $this->assertEquals('modified', $sut->apply('foo', $m, Mutation::SET));
    }

    public function testGetMutation(): void
    {
        $sut = new ModifierStub(Mutation::GET);
        $m = new VO;
        $this->assertEquals('modified', $sut->apply('foo', $m, Mutation::GET));
        $this->assertEquals('foo', $sut->apply('foo', $m, Mutation::SET));
    }

    public function testSetMutation(): void
    {
        $sut = new ModifierStub(Mutation::SET);
        $m = new VO;
        $this->assertEquals('modified', $sut->apply('foo', $m, Mutation::SET));
        $this->assertEquals('foo', $sut->apply('foo', $m, Mutation::GET));
    }
}
