<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Unit;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Stubs\Models\TermVOStub;

class TermVOTest extends TestCase
{
    #[Test]
    public function it_can_serialize_and_ignores_attributes(): void
    {
        $sut = new TermVOStub([
            'STRM' => 'T1234',
            'TERM_BEGIN_DT' => '2022-01-01',
            'TERM_END_DT' => '2022-05-01',
            'test' => 'test',
        ]);
        $s = serialize($sut);
        $sut2 = unserialize($s);
        $this->assertInstanceOf(Carbon::class, $sut2->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $sut2->TERM_END_DT);
        $this->assertEquals('foo bartest', $sut2->test);
    }
}
