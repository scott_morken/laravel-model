<?php

namespace Tests\Smorken\Model\Unit;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Stubs\Models\ModelWithPropertyModifiersStub;

class ModelWithPropertyModifiersTest extends TestCase
{
    public function testCanSerialize(): void
    {
        $sut = (new ModelWithPropertyModifiersStub)->forceFill([
            'foo' => 'fooval',
            'fiz' => 'fizval',
        ]);
        $s = serialize($sut);
        $sut2 = unserialize($s);
        $this->assertEquals([
            'foo' => 'fooval foo',
            'fiz' => 'fizval fiz',
        ], $sut2->toArray());
    }

    public function testGetModifiersApplied(): void
    {
        $sut = (new ModelWithPropertyModifiersStub)->forceFill([
            'foo' => 'fooval',
            'fiz' => 'fizval',
        ]);
        $this->assertEquals('fooval foo', $sut->foo);
        $this->assertEquals('fizval fiz', $sut->fiz);
        $this->assertEquals([
            'foo' => 'fooval foo',
            'fiz' => 'fizval fiz',
        ], $sut->toArray());
    }

    public function testGetModifiersAppliedSkipsCacheWhenDirty(): void
    {
        $sut = (new ModelWithPropertyModifiersStub)->forceFill([
            'foo' => 'fooval',
            'fiz' => 'fizval',
        ]);
        $this->assertEquals('fooval foo', $sut->foo);
        $this->assertEquals('fizval fiz', $sut->fiz);
        $this->assertEquals([
            'foo' => 'fooval foo',
            'fiz' => 'fizval fiz',
        ], $sut->toArray());
        $sut->foo = 'newval';
        $this->assertEquals('newval foo', $sut->foo);
        $this->assertEquals([
            'foo' => 'newval foo',
            'fiz' => 'fizval fiz',
        ], $sut->toArray());
    }
}
