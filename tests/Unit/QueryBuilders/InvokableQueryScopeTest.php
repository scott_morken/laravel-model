<?php

namespace Tests\Smorken\Model\Unit\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\QueryBuilders\InvokableQueryScope;
use Tests\Smorken\Model\Stubs\Models\ModelWithBuilderStub;
use Tests\Smorken\Model\Unit\TestCaseWithMockConnectionResolver;

class InvokableQueryScopeTest extends TestCaseWithMockConnectionResolver
{
    public function testBuilderFromClosure(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $sut = new InvokableQueryScope(static fn (Builder $query, mixed $value) => $query->where('biz', '=', $value),
            'biz');
        $query = $sut($query);
        $this->assertEquals('select * from `model_with_builder_stubs` where `biz` = ?', $query->toSql());
    }

    public function testSimpleAppliesFilter(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $sut = new InvokableQueryScope('isFooBar', 'biz');
        $query = $sut($query);
        $this->assertEquals('select * from `model_with_builder_stubs` where `foo` = ?', $query->toSql());
    }

    public function testSimpleSkipsFilter(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $sut = new InvokableQueryScope('isFooBar', 'biz');
        $sut->setShouldContinueClosure(fn (mixed $value): bool => $value !== 'biz');
        $query = $sut($query);
        $this->assertEquals('select * from `model_with_builder_stubs`', $query->toSql());
    }
}
