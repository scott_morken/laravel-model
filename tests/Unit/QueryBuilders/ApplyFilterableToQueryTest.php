<?php

namespace Tests\Smorken\Model\Unit\QueryBuilders;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Smorken\Model\Filters\Filterable;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\ApplyFilterableToQuery;
use Smorken\Support\Filter;
use Tests\Smorken\Model\Stubs\Models\ModelWithBuilderStub;
use Tests\Smorken\Model\Unit\TestCaseWithMockConnectionResolver;

class ApplyFilterableToQueryTest extends TestCaseWithMockConnectionResolver
{
    public function testFilterWithClosure(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $filter = new Filter([
            'f_active' => 1,
            'f_bars' => ['b1', 'b2'],
            'f_fooBar' => 'fb',
        ]);
        $filterable = new Filterable(
            $filter,
            new Collection([
                new FilterHandler('f_active', 'filterActive'),
                new FilterHandler('f_bars', 'filterBarIn'),
                new FilterHandler(
                    'f_fooBar',
                    static fn (Builder $query, mixed $value): Builder => $query->where('not_foo', '=', $value)
                ),
            ])
        );
        $sut = new ApplyFilterableToQuery($filterable);
        $query = $sut->apply($query);
        $this->assertEquals('select * from `model_with_builder_stubs` where `active` = ? and `bar` in (?, ?) and `not_foo` = ?',
            $query->toSql());
    }

    public function testSimpleFilter(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $filter = new Filter([
            'f_active' => 1,
            'f_bars' => ['b1', 'b2'],
            'f_fooBar' => 'fb',
        ]);
        $filterable = new Filterable(
            $filter,
            new Collection([
                new FilterHandler('f_active', 'filterActive'),
                new FilterHandler('f_bars', 'filterBarIn'),
                new FilterHandler('f_fooBar', 'filterFooBar'),
            ])
        );
        $sut = new ApplyFilterableToQuery($filterable);
        $query = $sut->apply($query);
        $this->assertEquals('select * from `model_with_builder_stubs` where `active` = ? and `bar` in (?, ?) and `foo` = ?',
            $query->toSql());
    }

    public function testSimpleFilterCanSkipEmptyArray(): void
    {
        $query = (new ModelWithBuilderStub)->newQuery();
        $filter = new Filter([
            'f_active' => 1,
            'f_bars' => [],
            'f_fooBar' => 'fb',
        ]);
        $filterable = new Filterable(
            $filter,
            new Collection([
                new FilterHandler('f_active', 'filterActive'),
                new FilterHandler('f_bars', 'filterBarIn'),
                new FilterHandler('f_fooBar', 'filterFooBar'),
            ])
        );
        $sut = new ApplyFilterableToQuery($filterable);
        $query = $sut->apply($query);
        $this->assertEquals('select * from `model_with_builder_stubs` where `active` = ? and `foo` = ?',
            $query->toSql());
    }
}
