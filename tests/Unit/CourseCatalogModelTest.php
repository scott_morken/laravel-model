<?php

namespace Tests\Smorken\Model\Unit;

use Tests\Smorken\Model\Stubs\Models\CourseCatalogModelStub;

class CourseCatalogModelTest extends TestCaseWithMockConnectionResolver
{
    public function testCourseOfferingsRelation(): void
    {
        $model = (new CourseCatalogModelStub)->forceFill([
            'catalog_catalog_id' => 'catid_1', 'catalog_course_id' => 'courseid_1',
        ]);
        $sut = $model->courseOfferings();
        $this->assertEquals(
            'select * from `CRSE_OFFER` where `CRSE_OFFER`.`offer_catalog_id` = ? and `CRSE_OFFER`.`offer_course_id` = ?',
            $sut->toSql()
        );
        $this->assertEquals(['catid_1', 'courseid_1'], $sut->getBindings());
    }
}
