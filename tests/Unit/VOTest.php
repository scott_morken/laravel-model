<?php

namespace Tests\Smorken\Model\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Model\VO;

class VOTest extends TestCase
{
    public function testMissingAttributeIsNull(): void
    {
        $sut = new VO(['id' => 1]);
        $this->assertNull($sut->foo);
    }
}
