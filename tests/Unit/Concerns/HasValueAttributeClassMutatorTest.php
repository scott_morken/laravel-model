<?php

namespace Tests\Smorken\Model\Unit\Concerns;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\ArrayCache\Repository;
use Smorken\Model\Attributes\Helpers\AttributesProvider;
use Smorken\Model\Attributes\ModifyValueAttribute;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Concerns\HasValueAttributeClassMutator;

class HasValueAttributeClassMutatorTest extends TestCase
{
    public function testOneInstanceRetrievesFromCacheAfterLoading(): void
    {
        $sut = $this->getSut();
        $attributesProvider = m::mock(AttributesProvider::class);
        $sut->setAttributesProvider(ModifyValuesAttribute::class, $attributesProvider);
        $sut->setAttributesProvider(ModifyValueAttribute::class, $attributesProvider);
        $attributesProvider->expects()
            ->getClassLevelAttributes()
            ->andReturn([]);
        $attributesProvider->expects()
            ->getPropertyLevelAttributes('foo')
            ->andReturn([]);
        $r1 = $sut->get('foo');
        $r2 = $sut->get('foo');
        $this->assertEquals([], $r1);
        $this->assertEquals([], $r2);
    }

    public function testTwoInstancesRetrievesFromCacheAfterLoading(): void
    {
        $sut = $this->getSut();
        $attributesProvider = m::mock(AttributesProvider::class);
        $sut->setAttributesProvider(ModifyValuesAttribute::class, $attributesProvider);
        $sut->setAttributesProvider(ModifyValueAttribute::class, $attributesProvider);
        $sut2 = $this->getSut();
        $sut2->setAttributesProvider(ModifyValuesAttribute::class, $attributesProvider);
        $sut2->setAttributesProvider(ModifyValueAttribute::class, $attributesProvider);
        $attributesProvider->expects()
            ->getClassLevelAttributes()
            ->andReturn([]);
        $attributesProvider->expects()
            ->getPropertyLevelAttributes('foo')
            ->andReturn([]);
        $r1 = $sut->get('foo');
        $r2 = $sut2->get('foo');
        $this->assertEquals([], $r1);
        $this->assertEquals([], $r2);
    }

    protected function getSut(): object
    {
        return new class
        {
            use HasValueAttributeClassMutator;

            public function get(string $key): array
            {
                return $this->getModifyValueByAttributes($key);
            }

            public function setAttributesProvider(string $attributeClass, mixed $provider): void
            {
                $this->attributesProviders[$attributeClass] = $provider;
            }
        };
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Repository::reset();
        m::close();
    }
}
