<?php

namespace Tests\Smorken\Model\Unit;

use Tests\Smorken\Model\Stubs\Models\ModelWithIdIsScopeStub;

class ModelWithIdIsScopeTest extends TestCaseWithMockConnectionResolver
{
    public function testArrayKey(): void
    {
        $model = new ModelWithIdIsScopeStub;
        $sut = $model->newQuery()->idIs(['id' => 1, 'other' => 99]);
        $this->assertEquals('select * from `model_with_id_is_scope_stubs` where (`id` = ? and `other` = ?)', $sut->toSql());
        $this->assertEquals([1, 99], $sut->getBindings());
    }
}
