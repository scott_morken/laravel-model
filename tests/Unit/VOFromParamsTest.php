<?php

namespace Tests\Smorken\Model\Unit;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Model\Stubs\Models\VOFromParamsStub;

class VOFromParamsTest extends TestCase
{
    public function testCanInstantiate(): void
    {
        $sut = new VOFromParamsStub(1, 'fizz buzz');
        $this->assertEquals(1, $sut->id);
        $this->assertEquals('fizz buzz', $sut->fooBar);
        $this->assertEquals(1, $sut['id']);
        $this->assertEquals('fizz buzz', $sut['fooBar']);
    }
}
