<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Unit;

use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Model\Stubs\Models\ModelWithoutBuilderStub;

class ModelWithoutBuilderTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_can_call_scoped_default_order(): void
    {
        $sut = new ModelWithoutBuilderStub;
        $q = $sut->newQuery()->defaultOrder();
        $this->assertEquals('select * from `model_without_builder_stubs` order by `foo` asc', $q->toSql());
    }

    #[Test]
    public function it_can_call_scoped_default_withs(): void
    {
        $sut = new ModelWithoutBuilderStub;
        $q = $sut->newQuery()->defaultWiths();
        $this->pdo->expects()
            ->prepare('select * from `model_without_builder_stubs`')
            ->andReturns($this->statement);
        $this->statement->allows()
            ->execute();
        $this->statement->expects()
            ->fetchAll()
            ->andReturns([
                ['id' => 1, 'bar_id' => 99, 'foo' => 'abc'],
            ]);
        $this->pdo->expects()
            ->prepare('select * from `model_with_builder_stubs` where `model_with_builder_stubs`.`id` in (99)')
            ->andReturns($this->statement);
        $this->statement->expects()
            ->fetchAll()
            ->andReturns([
                ['id' => 99],
            ]);
        $r = $q->get();
        $this->assertEquals(1, $r->first()->id);
    }
}
