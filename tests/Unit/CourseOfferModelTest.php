<?php

namespace Tests\Smorken\Model\Unit;

use Tests\Smorken\Model\Stubs\Models\CourseCatalogModelStub;
use Tests\Smorken\Model\Stubs\Models\CourseOfferModelStub;

class CourseOfferModelTest extends TestCaseWithMockConnectionResolver
{
    public function testCourseCatalogRelation(): void
    {
        $model = (new CourseOfferModelStub)->forceFill([
            'offer_catalog_id' => 'catid_1', 'offer_course_id' => 'courseid_1',
        ]);
        $sut = $model->courseCatalog();
        $this->assertEquals('select * from `CRSE_CATALOG` where `CRSE_CATALOG`.`catalog_catalog_id` = ? and `CRSE_CATALOG`.`catalog_course_id` = ?',
            $sut->toSql());
        $this->assertEquals(['catid_1', 'courseid_1'], $sut->getBindings());
    }

    public function testMultiKeyJoin(): void
    {
        $sut = (new CourseOfferModelStub)->newQuery()
            ->multiKeyJoin((new CourseCatalogModelStub)->getTable(), [
                'catalog_catalog_id' => 'offer_catalog_id',
                'catalog_course_id' => 'offer_course_id',
            ]);
        $this->assertEquals(
            'select * from `CRSE_OFFER` inner join `CRSE_CATALOG` on `CRSE_CATALOG`.`catalog_catalog_id` = `CRSE_OFFER`.`offer_catalog_id` and `CRSE_CATALOG`.`catalog_course_id` = `CRSE_OFFER`.`offer_course_id`',
            $sut->toSql()
        );
    }

    public function testMultiKeyJoinFromScope(): void
    {
        $sut = (new CourseOfferModelStub)->newQuery()->joinCourseCatalog();
        $this->assertEquals(
            'select * from `CRSE_OFFER` inner join `CRSE_CATALOG` on `CRSE_CATALOG`.`catalog_catalog_id` = `CRSE_OFFER`.`offer_catalog_id` and `CRSE_CATALOG`.`catalog_course_id` = `CRSE_OFFER`.`offer_course_id`',
            $sut->toSql()
        );
    }
}
