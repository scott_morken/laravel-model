<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Phpstan;

use Smorken\Model\Eloquent;
use Smorken\Model\QueryBuilders\Builder;

class EloquentStan extends Eloquent {}

$m = new EloquentStan(['foo' => 'bar']);
assert($m->foo === 'bar');
$q = $m->newQuery()->defaultOrder();
assert($q instanceof Builder);
