<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Phpstan;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Eloquent;
use Smorken\Model\QueryBuilders\Builder;

class EloquentStanWithBuilder extends Eloquent
{
    /** @use HasBuilder<EloquentStanBuilder<static>> */
    use HasBuilder;

    protected static string $builder = EloquentStanBuilder::class;
}

/**
 * @template TModel of Eloquent
 *
 * @extends Builder<TModel>
 */
class EloquentStanBuilder extends Builder
{
    /**
     * @return $this
     */
    public function defaultOrder(): self
    {
        /** @var $this */
        return $this->orderBy('foo');
    }

    public function something(): self
    {
        return $this;
    }
}

$m = new EloquentStanWithBuilder(['foo' => 'bar']);
$q = $m->newQuery()->defaultOrder();
$q = $q->something();
assert($q instanceof EloquentStanBuilder);
