<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Phpstan;

use Smorken\Model\VOFromParams;

class VOFromParamsStan extends VOFromParams
{
    public string $foo = 'bar';
}

$vo = new VOFromParamsStan;
assert($vo->foo === 'bar');
