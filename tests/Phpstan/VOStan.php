<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Phpstan;

use Smorken\Model\VO;

class VOStan extends VO {}

$vo = new VOStan(['foo' => 'bar']);
assert($vo->foo === 'bar');
