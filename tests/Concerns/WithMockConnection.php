<?php

namespace Tests\Smorken\Model\Concerns;

use Illuminate\Database\ConnectionInterface;
use Mockery as m;

trait WithMockConnection
{
    protected \PDO|m\MockInterface|null $pdo = null;

    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $this->pdo = m::mock(\PDO::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$this->pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function getMySqlConnectionConfig(): array
    {
        return [
            'driver' => 'mysql',
            'url' => null,
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => null,
            'username' => '',
            'password' => '',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                \PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ];
    }
}
