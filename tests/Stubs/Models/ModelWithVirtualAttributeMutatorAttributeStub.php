<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Eloquent;
use Tests\Smorken\Model\Stubs\Modifiers\ModifierStub;

#[ModifyValuesAttribute(new Modifiers(['foo' => new ModifierStub]))]
class ModelWithVirtualAttributeMutatorAttributeStub extends Eloquent
{
    protected array $addAttributeMutatedAttributes = ['foo'];
}
