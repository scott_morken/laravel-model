<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Eloquent;
use Smorken\Model\QueryBuilders\Concerns\HasIdIsScope;

class ModelWithIdIsScopeStub extends Eloquent
{
    use HasIdIsScope;

    protected $primaryKey = ['id', 'other'];
}
