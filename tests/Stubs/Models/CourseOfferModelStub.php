<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Eloquent;
use Smorken\Model\QueryBuilders\Concerns\Scopes\HasMultiKeyJoin;

class CourseOfferModelStub extends Eloquent
{
    use Compoships, HasMultiKeyJoin;

    protected $table = 'CRSE_OFFER';

    public function courseCatalog(): BelongsTo
    {
        return $this->belongsTo(
            CourseCatalogModelStub::class,
            ['offer_catalog_id', 'offer_course_id'],
            ['catalog_catalog_id', 'catalog_course_id']
        );
    }

    public function scopeJoinCourseCatalog(Builder $query): Builder
    {
        return $query->multiKeyJoin((new CourseCatalogModelStub)->getTable(), ['catalog_catalog_id' => 'offer_catalog_id', 'catalog_course_id' => 'offer_course_id']);
    }
}
