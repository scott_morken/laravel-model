<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Eloquent;
use Tests\Smorken\Model\Stubs\Modifiers\TermModifiersStub;

#[ModifyValuesAttribute(new TermModifiersStub)]
class TermModelStub extends Eloquent
{
    public $incrementing = false;
}
