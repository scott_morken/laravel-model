<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Eloquent;
use Tests\Smorken\Model\Stubs\Builders\QueryBuilderStub;

class ModelWithBuilderStub extends Eloquent
{
    protected static string $builder = QueryBuilderStub::class;
}
