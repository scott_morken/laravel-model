<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\VOFromParams;

class VOFromParamsStub extends VOFromParams
{
    public function __construct(
        public int $id,
        public string $fooBar
    ) {}
}
