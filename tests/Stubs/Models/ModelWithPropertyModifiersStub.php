<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\Eloquent;
use Tests\Smorken\Model\Stubs\Modifiers\PropertyModifiersStub;

#[ModifyValuesAttribute(new PropertyModifiersStub)]
class ModelWithPropertyModifiersStub extends Eloquent {}
