<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Stubs\Models;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Eloquent;

class ModelWithoutBuilderStub extends Eloquent
{
    protected $fillable = ['bar_id' => 99, 'foo' => 'abc'];

    public function bar(): BelongsTo
    {
        return $this->belongsTo(ModelWithBuilderStub::class);
    }

    public function scopeDefaultOrder(EloquentBuilder $query): EloquentBuilder
    {
        return $query->orderBy('foo');
    }

    public function scopeDefaultWiths(EloquentBuilder $query): EloquentBuilder
    {
        return $query->with('bar');
    }
}
