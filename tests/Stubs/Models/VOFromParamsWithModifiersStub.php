<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Carbon\Carbon;
use Smorken\Model\Attributes\Modifiers\CarbonModifier;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Model\Attributes\ModifyValueAttribute;
use Smorken\Model\Constants\Mutation;
use Smorken\Model\VOFromParams;

class VOFromParamsWithModifiersStub extends VOFromParams
{
    public function __construct(
        public int $id,
        #[ModifyValueAttribute(new CarbonModifier(Mutation::SET))]
        public string|Carbon $today,
        #[ModifyValueAttribute(new TrimModifier(Mutation::SET))]
        public string $someText
    ) {
        parent::__construct();
    }
}
