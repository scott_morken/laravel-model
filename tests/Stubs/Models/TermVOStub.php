<?php

declare(strict_types=1);

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\VO;
use Tests\Smorken\Model\Stubs\Modifiers\TermModifiersStub;

#[ModifyValuesAttribute(new TermModifiersStub)]
class TermVOStub extends VO {}
