<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\CarbonModifier;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Model\VO;

#[ModifyValuesAttribute(new Modifiers([
    'foo' => new TrimModifier,
    'bar' => new CarbonModifier,
]))]
class ModelWithArrayModifiersStub extends VO {}
