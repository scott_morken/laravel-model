<?php

namespace Tests\Smorken\Model\Stubs\Models;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Eloquent;

class CourseCatalogModelStub extends Eloquent
{
    use Compoships;

    protected $table = 'CRSE_CATALOG';

    public function courseOfferings(): HasMany
    {
        return $this->hasMany(
            CourseOfferModelStub::class,
            ['offer_catalog_id', 'offer_course_id'],
            ['catalog_catalog_id', 'catalog_course_id']
        );
    }
}
