<?php

namespace Tests\Smorken\Model\Stubs\Builders;

use Illuminate\Support\Collection;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;

class QueryBuilderStub extends Builder
{
    public function filterActive(int $active): self
    {
        return $this->where('active', '=', $active);
    }

    public function filterBarIn(array $bars): self
    {
        if ($bars) {
            $this->whereIn('bar', $bars);
        }

        return $this;
    }

    public function filterFooBar(string $bar): self
    {
        return $this->isFooBar($bar);
    }

    public function isFooBar(string $bar): self
    {
        return $this->where('foo', '=', $bar);
    }

    protected function getFilterHandlers(): iterable
    {
        return new Collection([
            new FilterHandler('f_fooBar', 'filterFooBar'),
        ]);
    }
}
