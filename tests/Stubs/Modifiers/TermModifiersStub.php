<?php

namespace Tests\Smorken\Model\Stubs\Modifiers;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\CarbonModifier;
use Smorken\Model\Contracts\Model;

class TermModifiersStub extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'TERM_BEGIN_DT' => new CarbonModifier,
                'TERM_END_DT' => new CarbonModifier,
                'test' => fn (mixed $v, Model $model) => 'foo bar'.$v,
            ],
            'set' => [],
        ];
    }
}
