<?php

namespace Tests\Smorken\Model\Stubs\Modifiers;

use Smorken\Model\Attributes\Modifiers\BaseModifier;
use Smorken\Model\Contracts\Model;

class ModifierStub extends BaseModifier
{
    protected function modify(mixed $value, Model $model): string
    {
        return 'modified';
    }
}
