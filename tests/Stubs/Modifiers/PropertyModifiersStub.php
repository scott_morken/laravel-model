<?php

namespace Tests\Smorken\Model\Stubs\Modifiers;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\CarbonModifier;
use Smorken\Model\Attributes\Modifiers\ViaCallbackModifier;
use Smorken\Model\Contracts\Model;

class PropertyModifiersStub extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'foo' => new ViaCallbackModifier(fn (string $v, Model $m): string => $v.' foo'),
                'fiz' => fn (string $v, Model $m): string => $v.' fiz',
            ],
            'set' => [
                'bar' => new CarbonModifier,
            ],
        ];
    }
}
