<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 11:04 AM
 */
include __DIR__.'/vendor/autoload.php';

use Phpro\SoapClient\Type\RequestInterface;

class SFA_GET_STUDENT_AWARDS_REQ implements RequestInterface
{
    /**
     * @var STUDENT_AWARD_REQUEST
     */
    protected $STUDENT_AWARD_REQUEST = null;

    /**
     * @var anonymous7
     */
    protected $languageCd = null;

    /**
     * @var anonymous8
     */
    protected $SCC_PROFILE_ID = null;

    public function __construct(STUDENT_AWARD_REQUEST $AWARD_REQUEST, $SCC_PROFILE_ID = null, $languageCd = null)
    {
        $this->STUDENT_AWARD_REQUEST = $AWARD_REQUEST;
        $this->SCC_PROFILE_ID = $SCC_PROFILE_ID;
        $this->languageCd = $languageCd;
    }

    /**
     * @return STUDENT_AWARD_REQUEST
     */
    public function getSTUDENT_AWARD_REQUEST()
    {
        return $this->STUDENT_AWARD_REQUEST;
    }

    /**
     * @return anonymous7
     */
    public function getLanguageCd()
    {
        return $this->languageCd;
    }

    /**
     * @return anonymous8
     */
    public function getSCC_PROFILE_ID()
    {
        return $this->SCC_PROFILE_ID;
    }
}

class STUDENT_AWARD_REQUEST implements RequestInterface
{
    /**
     * @var EMPLID
     */
    protected $EMPLID = null;

    /**
     * @var INSTITUTION
     */
    protected $INSTITUTION = null;

    /**
     * @var AID_YEAR
     */
    protected $AID_YEAR = null;

    /**
     * @var SCC_ENTITY_INST_ID
     */
    protected $SCC_ENTITY_INST_ID = null;

    /**
     * @var anonymous6
     */
    protected $isDeleted = null;

    public function __construct($EMPLID, $INSTITUTION = null, $AID_YEAR = null)
    {
        $this->EMPLID = $EMPLID;
        $this->INSTITUTION = $INSTITUTION;
        $this->AID_YEAR = $AID_YEAR;
    }

    /**
     * @return EMPLID
     */
    public function getEMPLID()
    {
        return $this->EMPLID;
    }

    /**
     * @return INSTITUTION
     */
    public function getINSTITUTION()
    {
        return $this->INSTITUTION;
    }

    /**
     * @return AID_YEAR
     */
    public function getAID_YEAR()
    {
        return $this->AID_YEAR;
    }

    /**
     * @return SCC_ENTITY_INST_ID
     */
    public function getSCC_ENTITY_INST_ID()
    {
        return $this->SCC_ENTITY_INST_ID;
    }

    /**
     * @return anonymous6
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
}

$sar = new STUDENT_AWARD_REQUEST(35765528, null, 2018);
$req = new SFA_GET_STUDENT_AWARDS_REQ($sar);

$model = new \Smorken\Model\Soap\Phpro\Soap\Model;
$model->setWsdl(config('sis.soap.financial_aid.wsdl'));
$model->setSoapOptions(config('model.soap.soap_options'));

$action_mod = function ($value) {
    $str = config('sis.soap.financial_aid.middleware.soap_action');

    return sprintf($str, $value);
};

$wsse = new \Smorken\Model\Soap\Wsse\WsseSoapHeader('ALE2159987');

try {
    $results = $model->newRequest()
        ->asFunction(config('sis.soap.financial_aid.function'), $req)
        ->soapHeader($wsse)
        ->modifier('action', $action_mod)
        ->saveLastSoapRequest()
        ->run();
    print_r($results);
} catch (\Exception $e) {
    print_r($e);
}
//print_r($model->getLastSoapRequest());

function config($key, $default = null)
{
    $data = [
        'sis.soap.financial_aid.middleware.wsse' => 'alt_id',
        'sis.soap.financial_aid.middleware.soap_action' => '#%s#MC_ADM_APPL_NODE#{V1.1}CuXg6Li3ON35MiJulnKlMw==',
        'sis.soap.financial_aid.function' => 'SFA_GET_STUDENT_AWARDS',
        'sis.soap.financial_aid.wsdl' => 'https://csopdev.maricopa.edu/PSIGW/PeopleSoftServiceListeningConnector/SFA_FINANCIAL_AID.1.wsdl',
        'model.soap.soap_options' => ['cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1],
        'model.soap.client_options' => [],
    ];

    return array_get($data, $key, $default);
}
